import java.util.Scanner;

class Fahrkartenautomat4 {
    public static void main(String[] args) {
        @SuppressWarnings("resource")
		Scanner tastatur = new Scanner(System.in);

        int anzahlTickets;
        double zuZahlenderBetrag;
        double ticketpreis = 0;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        //double eingegebenerBetrag;

        //Ticketauswahl
    	System.out.println("Bitte w�hlen Sie ein Ticket");
		System.out.println("1) Einzelfahrschein Regeltarif AB [2,90 EUR]");
		System.out.println("2) Tageskarte Regeltarif AB [8,60 EUR]");
		System.out.println("3) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");
		System.out.println("4) Ende");
		
		System.out.print("\nIhre Auswahl:");
		
		int auswahl = tastatur.nextInt();
		
		if (auswahl == 1) {
			System.out.println("Sie habe Ticket A gew�hlt");
			ticketpreis = 2.90;
		}
		if (auswahl == 2) {
			System.out.println("Sie habe Ticket B gew�hlt");
			ticketpreis = 8.60;
		}
		if (auswahl == 3) {
			System.out.println("Sie habe Ticket C gew�hlt");
			ticketpreis = 23.50;
		}
		if (auswahl == 4) {
			System.out.println("Ihr Vorgang wird abgebrochen ");
			for (int i = 0; i < 10; i++) {
	            System.out.print("=");
	            try {
	                Thread.sleep(250);
	            } catch (InterruptedException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	      
	        }
			
			 System.out.println("\n");
			
			System.out.println("M�chten Sie das Programm neustarten? j/n? ");
			String restart = tastatur.next();
			if(restart.equals("j"))
				main(args);
			else{
				System.out.println("Das Programm wird beendet ");
				for (int i = 0; i < 10; i++) {
		            System.out.print("=");
		            try {
		                Thread.sleep(250);
		            } catch (InterruptedException e) {
		                // TODO Auto-generated catch block
		                e.printStackTrace();
		            }
		        }
				System.out.println("\n");
				System.out.println("Das Programm wurde beendet");
				System.exit(0);
			}
		}
		if (auswahl >= 5) {
			System.out.println("Fehler! Kehre zum Men� zur�ck ");
			for (int i = 0; i < 10; i++) {
	            System.out.print("=");
	            try {
	                Thread.sleep(250);
	            } catch (InterruptedException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
	        System.out.println("\n\n");
			main(args);
		}
			
		System.out.println("Ticketpreis (EURO-Cent): " + ticketpreis +"0" + "�");

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        zuZahlenderBetrag = ticketpreis * anzahlTickets;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 10; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        
        if (rueckgabebetrag > 0.0) {
        	System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\n");
        
        System.out.println("M�chten Sie Ihren Einkauf fortsetzen? j/n? ");
		String restart = tastatur.next();
		
		if(restart.equals("j"))
			main(args);
		else{
			System.out.println("\nVielen Dank f�r Ihren Einkauf!" + "\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
	                + "Wir w�nschen Ihnen eine gute Fahrt.");
			System.exit(0);
		}
          
    }
 
}
