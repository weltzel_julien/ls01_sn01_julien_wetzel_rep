package angestellte;
public class Ladung1 {

	private String bezeichnung;
	private int menge;
	
	public Ladung1() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Ladung1(String bezeichnung, int menge) {
		super();
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	
	public void addLadung1(Ladung1 neueLadung1) {
		neueLadung1.setBezeichnung("Baumwolle");
		neueLadung1.setMenge(90);
			
		System.out.println(neueLadung1.getBezeichnung());
		System.out.println(neueLadung1.getMenge());
	}
	
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	public static void main (String [] args) {
		
		Ladung1 neueLadung1 = new Ladung1();
	
		neueLadung1.addLadung1(neueLadung1);
		
	}
	
}
	
