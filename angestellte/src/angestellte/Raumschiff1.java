package angestellte; 
import java.util.ArrayList;

public class Raumschiff1 {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static String broadcastKommunikator;
	private Ladung ladungsverzeichnis;
	
	public Raumschiff1() {
		super();
	}
	
	public Raumschiff1(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	ArrayList<String> y = new ArrayList<String>();
	ArrayList<Ladung> z = new ArrayList<Ladung>();
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	public void photonentorpedoSchiessen(Raumschiff1 r) {
		if (photonentorpedoAnzahl <= 0) {
			System.out.println("-=*Click*=-");
		} else {
				photonentorpedoAnzahl--;
		System.out.println("Photonentorpedo abgeschossen");
		r.treffer(r);
		//nachrichtenAnAlle();//
		}
	}
		
	public void phaserkanoneSchiessen(Raumschiff1 r) {
		if (energieversorgungInProzent < 50) {
			System.out.println("-=*Click*=-");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
		System.out.println("Phaserkanone abgeschossen");
		r.treffer(r);
		//nachrichtenAnAlle();//
		}
	}
	
	private void treffer(Raumschiff1 r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen");
	}

	public void nachrichtenAnAlle(String message) {
		
	}
	
	public void eintraegeLogbuchZurueckgeben() {
		ArrayList<String> y = new ArrayList<String>();
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		
	}
	
	public void addLadung1(Ladung1 neueLadung) {
		neueLadung.setBezeichnung("Baumwolle");
		neueLadung.setMenge(90);
		
		System.out.println(neueLadung.getBezeichnung());
		System.out.println(neueLadung.getMenge());
	}
	
	public void zustandRaumschiff(Raumschiff1 r) {
		r.setAndroidenAnzahl(3);
		r.setEnergieversorgungInProzent(80);
		r.setHuelleInProzent(70);
		r.setLebenserhaltungssystemeInProzent(90);
		r.setPhotonentorpedoAnzahl(5);
		r.setSchiffsname("Andromeda");
		r.setSchildeInProzent(50);
		
		System.out.println(r.getAndroidenAnzahl());
		System.out.println(r.getEnergieversorgungInProzent());
		System.out.println(r.getHuelleInProzent());
		System.out.println(r.getLebenserhaltungssystemeInProzent());
		System.out.println(r.getPhotonentorpedoAnzahl());
		System.out.println(r.getSchiffsname());
		System.out.println(r.getSchildeInProzent());
	}

	public void ladungsverzeichnisAusgeben() {
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
	}

	public static void main (String [] args) {
		
		Raumschiff1 r = new Raumschiff1();
		Ladung1 neueLadung = new Ladung1();
		
		r.zustandRaumschiff(r);
		r.phaserkanoneSchiessen(r);
		r.photonentorpedoSchiessen(r); 
		
		neueLadung.addLadung1(neueLadung);
	
	}

		public static void main (String [] args) {
		
		Raumschiff klingonen = new Raumschiff();
		Raumschiff romulaner = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		
	    Ladung klingonenLadung = new Ladung();
		Ladung romulanerLadung = new Ladung();
		Ladung vulkanierLadung = new Ladung();
				
		
		klingonen.photonentorpedoSchiessen(romulaner);			
		romulaner.phaserkanoneSchiessen(klingonen);
		System.out.println("Vulkanier: Gewalt ist nicht logisch");
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
		System.out.println("Zustand Klingonen: ");
		klingonen.setPhotonentorpedoAnzahl(1);
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setSchildeInProzent(100);
		klingonen.setHuelleInProzent(100);
		klingonen.setLebenserhaltungssystemeInProzent(100);
		klingonen.setSchiffsname("IKS Hegh'ta");
		klingonen.setAndroidenAnzahl(2);
		
		System.out.println(klingonen.getPhotonentorpedoAnzahl());
		System.out.println(klingonen.getEnergieversorgungInProzent());
		System.out.println(klingonen.getSchildeInProzent()); 
		System.out.println(klingonen.getHuelleInProzent());
		System.out.println(klingonen.getLebenserhaltungssystemeInProzent());
		System.out.println(klingonen.getSchiffsname());
		System.out.println(klingonen.getAndroidenAnzahl());

		System.out.println("Ladung Klingonen: ");
		klingonenLadung.setBezeichnung("Ferengi Schneckensaft");
		klingonenLadung.setMenge(200);
		System.out.println(klingonenLadung.getBezeichnung());
		System.out.println(klingonenLadung.getMenge());
		klingonenLadung.setBezeichnung("Bat'leth Klingonen Schwert");
		klingonenLadung.setMenge(200);
		System.out.println(klingonenLadung.getBezeichnung());
		System.out.println(klingonenLadung.getMenge());
	
		System.out.println("Zustand Romulaner: ");
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(100);
		romulaner.setSchildeInProzent(100);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebenserhaltungssystemeInProzent(100);
		romulaner.setSchiffsname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);
		
		System.out.println(romulaner.getPhotonentorpedoAnzahl());
		System.out.println(romulaner.getEnergieversorgungInProzent());
		System.out.println(romulaner.getSchildeInProzent()); 
		System.out.println(romulaner.getHuelleInProzent());
		System.out.println(romulaner.getLebenserhaltungssystemeInProzent());
		System.out.println(romulaner.getSchiffsname());
		System.out.println(romulaner.getAndroidenAnzahl());

		System.out.println("Ladung Romulaner: ");
		romulanerLadung.setBezeichnung("Borg-Schrott");
		romulanerLadung.setMenge(5);
		System.out.println(romulanerLadung.getBezeichnung());
		System.out.println(romulanerLadung.getMenge());
		romulanerLadung.setBezeichnung("Rote Materie");
		romulanerLadung.setMenge(2);
		System.out.println(romulanerLadung.getBezeichnung());
		System.out.println(romulanerLadung.getMenge());
		romulanerLadung.setBezeichnung("Plasma-Waffe");
		romulanerLadung.setMenge(50);
		System.out.println(romulanerLadung.getBezeichnung());
		System.out.println(romulanerLadung.getMenge());
	
		System.out.println("Zustand Vulkanier: ");
		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setSchildeInProzent(80);
		vulkanier.setHuelleInProzent(50);
		vulkanier.setLebenserhaltungssystemeInProzent(100);
		vulkanier.setSchiffsname("NI'Var");
		vulkanier.setAndroidenAnzahl(5);
		
		System.out.println(vulkanier.getPhotonentorpedoAnzahl());
		System.out.println(vulkanier.getEnergieversorgungInProzent());
		System.out.println(vulkanier.getSchildeInProzent()); 
		System.out.println(vulkanier.getHuelleInProzent());
		System.out.println(vulkanier.getLebenserhaltungssystemeInProzent());
		System.out.println(vulkanier.getSchiffsname());
		System.out.println(vulkanier.getAndroidenAnzahl());

		System.out.println("Ladung Vulkanier: ");
		vulkanierLadung.setBezeichnung("Forschugssonde");
		vulkanierLadung.setMenge(35);
		System.out.println(vulkanierLadung.getBezeichnung());
		System.out.println(vulkanierLadung.getMenge());
		vulkanierLadung.setBezeichnung("Photonentorpedo");
		vulkanierLadung.setMenge(3);
		System.out.println(vulkanierLadung.getBezeichnung());
		System.out.println(vulkanierLadung.getMenge());

				
		
	
	}

}



